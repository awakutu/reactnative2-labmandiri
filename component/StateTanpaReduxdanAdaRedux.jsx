import React, { Component } from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import Constants from "expo-constants";
import { useDispatch, useSelector } from "react-redux";

class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flexDirection: "row",
            width: 200,
            justifyContent: "space-around",
          }}
        >
          <TouchableOpacity onPress={() => this.props.increaseCounter()}>
            <Text style={{ fontSize: 20 }}>Increase</Text>
          </TouchableOpacity>
          <Text style={{ fontSize: 20 }}>{this.props.counter}</Text>
          <TouchableOpacity onPress={() => this.props.decreaseCounter()}>
            <Text style={{ fontSize: 20 }}>Decrease</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state)=> {
  return {
    counter: state.counter,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    increaseCounter: () => dispatch({ type: "INCREASE_COUNTER" }),
    decreaseCounter: () => dispatch({ type: "DECREASE_COUNTER" }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

export class StateTanpaRedux extends Component {
  state = {
    counter: 0,
  };

  increaseCounter = () => {
    this.setState({
      counter: this.state.counter + 1,
    });
  };

  decreaseCounter = () => {
    this.setState({
      counter: this.state.counter - 1,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flexDirection: "row",
            width: 200,
            justifyContent: "space-around",
          }}
        >
          <TouchableOpacity onPress={this.increaseCounter}>
            <Text style={{ fontSize: 20 }}>Increase</Text>
          </TouchableOpacity>
          <Text style={{ fontSize: 20 }}>{this.state.counter}</Text>
          <TouchableOpacity onPress={this.decreaseCounter}>
            <Text style={{ fontSize: 20 }}>Decrease</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: Constants.statusBarHeight,
    backgroundColor: "#ecf0f1",
    padding: 8,
  },
});
