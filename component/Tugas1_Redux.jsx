import React from "react";
import { Text, View, TextInput } from "react-native";
import { useDispatch, useSelector } from "react-redux";

const PizzaTranslator = () => {
  const PizzaReducer = useSelector((state) => state);
  const dispatch = useDispatch();
  return (
    <View style={{ padding: 10 }}>
          <TextInput
        style={{ height: 40 }}
        placeholder="Silakan ketik di sini untuk diterjemahkan"
        onChangeText={(value) => dispatch({type: 'TRANSLATE', text:value } ) } 
      />
      <Text style={{ paxding: 10, fontSize: 42 }}>
      {PizzaReducer.text}
      </Text>
      
    </View>
  );
};

export default PizzaTranslator