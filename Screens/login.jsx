import React, {useState} from "react";
import { View, Text, TextInput, Button, StyleSheet } from "react-native";

export default function login({ login }) {
const [username, setUsername] = useState("")
const [password, setPassword] = useState("")
    return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text> You Haven't logged in yet</Text>
      <TextInput
        style={styles.input}
        onChange={(e) => setUsername(e.target.value)}
        placeholder={"Masukkan username"}
      />
      <TextInput
        secureTextEntry={true}
        style={styles.input}
        onChange={(e) => setPassword(e.target.value)}
        placeholder={"Masukkan password"}
      />
      <Button title="Login" onPress={login} />
    </View>
  );
}

const styles = StyleSheet.create({
    input: {
      height: 27,
      width: 250,
      fontSize: 10,
      borderColor: "gray",
      borderWidth: 1,
      marginTop: 15,
    },
  });
  
