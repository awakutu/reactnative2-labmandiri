import React, { Component } from "react";
//import {default as CounterApp, StateTanpaRedux} from './component/StateTanpaReduxdanAdaRedux'; //Lab 2 & Lab 3
import {createStore} from 'redux'; //Lab 2
import {Provider} from 'react-redux'; //Lab 2
import {default as PizzaTranslator} from './component/LabTextInput' //Lab 3
import {default as PizzaTranslatorRedux} from './component/Tugas1_Redux' // Tugas 1
import UserAuth from './component/UserAuth'

//Lab 2
const initialState ={
  counter: 0
}

//Lab 2
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'INCREASE_COUNTER':
      return {counter: state.counter + 1}
    case 'DECREASE_COUNTER':
      return {counter: state.counter - 1}
  }
  return state
}      

//Lab 2
const store = createStore(reducer)

//Tugas 1
const initialStateTugas1 ={
  text: '-' 
}

//Tugas 1
const reducerTugas1 = (state = initialStateTugas1, action) => {
  switch (action.type) {
    case 'TRANSLATE':{
      return {
        text: state.text += state.text
           .split(" ")
           .map((word) => word && "🍕")
           .join(" ")
        } 
      }
      
  }
  return state ;
} 
       
//Tugas 1
const storeTugas1 = createStore(reducerTugas1)
 
//Lab 1, Lab 2 , Lab 3, Tugas 1, Lab 4
export default class Tugas1 extends Component {
  render() {
    return (
      // store untuk Lab, storeTugas1 untuk tugas
     <Provider store={storeTugas1}> 
       {/* <StateTanpaRedux/> */}
       {/* <CounterApp/> */}
       {/* <PizzaTranslator/> */}
       {/* <PizzaTranslatorRedux/> */}
      <UserAuth/>
     </Provider>
    )
  }
}



